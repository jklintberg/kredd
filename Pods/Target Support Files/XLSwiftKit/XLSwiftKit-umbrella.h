#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "UINavigationBar+Swift.h"
#import "XLFoundationSwiftKit-Bridgin-Header.h"
#import "XLFoundationSwiftKit.h"

FOUNDATION_EXPORT double XLSwiftKitVersionNumber;
FOUNDATION_EXPORT const unsigned char XLSwiftKitVersionString[];

