//
//  FirstViewController.swift
//  Kredd
//
//  Created by Jonatan Klintberg on 2017-09-14.
//  Copyright © 2017 Jonatan Klintberg. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import XLSwiftKit
import AVFoundation

class FirstViewController: UIViewController {
    
    @IBOutlet weak var noLoanImg: UIImageView!
    @IBOutlet weak var plusButton: UIButton!
    
    @IBOutlet weak var loanImgView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
//        let spin2 = NVActivityIndicatorView (frame: CGRect (x: 100, y: 100, width: 100, height: 100), type: .ballClipRotateMultiple, color: UIColor.red, padding: 0)
//        view.addSubview(spin2)
//        spin2.startAnimating()
        
        
        let playerLayer = videoPlayerLayer()
        playerLayer.frame = view.bounds.insetBy(dx: 40, dy: 40)
        view.layer.insertSublayer(playerLayer, at: 0)
        
        var f  = playerLayer.frame
        f.origin.y = f.origin.y + 40
        playerLayer.frame = f
        
        plusButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
//        playerLayer.frame.midY = plusButton.center.y
        
        let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        self.loanImgView.addGestureRecognizer(gestureRecognizer)
        self.loanImgView.alpha = 0
        self.noLoanImg.alpha = 1
        
        self.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        self.title = nil;
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tap(recognizer:)))
        view?.addGestureRecognizer(tapGesture)
    }
    
    func buttonAction(sender: UIButton!) {
        
        
    }
    
    func tap(recognizer: UITapGestureRecognizer) {
        getData()
    }
    
    func getData ()
    {
        var request = URLRequest(url: URL(string: "http://192.168.2.115:9090/loans/?user=00763078460")!)
        request.httpMethod = "GET"
    
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                DispatchQueue.main.async(execute: {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.loanImgView.alpha = 0
                        self.noLoanImg.alpha = 1
                    })
                })
                
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
                
                DispatchQueue.main.async(execute: {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.loanImgView.alpha = 0
                        self.noLoanImg.alpha = 1
                    })
                })
                
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
            
            DispatchQueue.main.async(execute: {
                UIView.animate(withDuration: 0.3, animations: {
                    self.loanImgView.alpha = 1
                     self.noLoanImg.alpha = 0
                })
            })
        }
        task.resume()
    }
    
    @IBAction func handlePan(_ gestureRecognizer: UIPanGestureRecognizer) {
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {
            
            let translation = gestureRecognizer.translation(in: self.view)
            // note: 'view' is optional and need to be unwrapped
            gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x + translation.x, y: gestureRecognizer.view!.center.y)
            gestureRecognizer.setTranslation(CGPoint.zero, in: self.view)
            
            if ((gestureRecognizer.view?.frame.origin.x)! > CGFloat(200)) {
                UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                    var f = gestureRecognizer.view?.frame
                    f?.origin.x = 400
                    gestureRecognizer.view?.frame = f!
                    self.noLoanImg.alpha = 1
                }, completion:nil)
            }
        }
    }
    
    fileprivate var player: AVPlayer? {
        didSet { player?.play() }
    }
    
    fileprivate var playerObserver: Any?
    
    deinit {
        guard let observer = playerObserver else { return }
        NotificationCenter.default.removeObserver(observer)
    }
    
    
    
    func videoPlayerLayer() -> AVPlayerLayer {
        
        let p = Bundle.main.path(forResource: "circle", ofType: "mp4")
        let fileURL = URL(fileURLWithPath: p!)
        let player = AVPlayer(url: fileURL)
        let resetPlayer = {
            player.seek(to: kCMTimeZero)
            player.play()
        }
        playerObserver = NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: nil) { notification in
            resetPlayer()
        }
        self.player = player
        return AVPlayerLayer(player: player)
    }
}
