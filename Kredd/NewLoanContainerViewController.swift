//
//  NewLoanPhoneViewController.swift
//  Kredd
//
//  Created by Jonatan Klintberg on 2017-09-15.
//  Copyright © 2017 Jonatan Klintberg. All rights reserved.
//

import UIKit

class NewLoanContainerViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var _scrollView: UIScrollView!

    @IBOutlet weak var vc1:NewLoanPhoneViewController?
    @IBOutlet weak var vc2:NewLoanInputViewController?
    @IBOutlet weak var view1:UIView?
    @IBOutlet weak var view2:UIView?
    
    @IBOutlet weak var backButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        nextButton.layer.cornerRadius = nextButton.frame.height / 2
        nextButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        backButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        nextButton.layer.shadowColor = UIColor.init(red: 61, green: 59, blue: 130).cgColor
        nextButton.layer.shadowOpacity = 0.3
        nextButton.layer.shadowOffset = CGSize (width: 0, height: 12)
        nextButton.layer.shadowRadius = 15.0
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tap(recognizer:)))
        view?.addGestureRecognizer(tapGesture)
    
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        vc1 = storyboard.instantiateViewController(withIdentifier: "NewLoanPhoneViewController") as? NewLoanPhoneViewController
        vc2 = (storyboard.instantiateViewController(withIdentifier: "NewLoanInputViewController") as! NewLoanInputViewController)
        
        view1 = vc1?.view
        view2 = vc2?.view
        
        _scrollView.addSubview(view1!)
        _scrollView.addSubview(view2!)
        
        let scrollTapGesture = UITapGestureRecognizer(target: self, action: #selector(scrollTap(recognizer:)))
        _scrollView?.addGestureRecognizer(scrollTapGesture)
        _scrollView.delegate = self
        _scrollView.contentSize = CGSize (width: view.frame.width * 2, height: view.frame.height)
        
        
        
    }
    
    override func viewDidLayoutSubviews() {
        
        let f = view.frame
        
        view1!.frame = f
        view2!.frame = CGRect (x: f.width, y: 0, width: f.width, height: f.height)
    }
    
    func tap(recognizer: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func scrollTap(recognizer: UITapGestureRecognizer) {
        print("scroll tapped")
        self.view.endEditing(true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (_scrollView.contentOffset.x < _scrollView.frame.size.width) {
            
            // Page 1
            
        } else {
            
            
        }
    }
    
    func goToSuccess () {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SuccessViewController") as? SuccessViewController
        vc?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
        self.present(vc!, animated: true, completion: nil)
    }
    
    func dismissButton (sender: UIButton!) {
        dismiss(animated: true, completion: nil)
    }

    func buttonAction(sender: UIButton!) {
        
        if (_scrollView.contentOffset.x < _scrollView.frame.size.width) {
            // Page 1
            _scrollView.setContentOffset(CGPoint (x: _scrollView.frame.size.width, y: 0), animated: true)
            
            
        } else {
            
            goToSuccess()
            
            let creditor = vc1?.phoneNumberField.text ?? "0046763078460"
            let amount = vc2?.amountField.text ?? "1000"
            let interest = vc2?.InterestField.text ?? "55"
            let payBackTime = vc2?.weeksField.text ?? "2"
            let sender = "+46763078460"
            
            var request = URLRequest(url: URL(string: "http://192.168.2.115:9090/loans/")!)
            request.httpMethod = "POST"
            
            let json: [String: Any] = ["Creditor": sender,
                                       "Debtor": creditor,
                                       "Principal": Int(amount) ?? 0,
                                       "Interest": Int(interest) ?? 0,
                                       "MaturityDate": Int(payBackTime) ?? 0,
                                       ]
            
            let jsonData = try? JSONSerialization.data(withJSONObject: json)
            
            
            
            
//            let postString = ""
//
//                    "Creditor=" + sender + "&" +
//                    "Debtor=" + creditor + "&" +
//                    "Principal=" + amount + "&" +
//                    "Interest=" + interest + "&" +
//                    "MaturityDate=" + payBackTime
            
            
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {                                                 // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(String(describing: response))")
                }
                
                let responseString = String(data: data, encoding: .utf8)
                print("responseString = \(String(describing: responseString))")
            }
            task.resume()
        }
        
    }
    
    
}


