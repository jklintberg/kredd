//
//  NewLoanPhoneViewController.swift
//  Kredd
//
//  Created by Jonatan Klintberg on 2017-09-15.
//  Copyright © 2017 Jonatan Klintberg. All rights reserved.
//

import UIKit

class NewLoanPhoneViewController: UIViewController {
    
    @IBOutlet weak var phoneNumberField: UITextField!
    @IBOutlet weak var textField: UITextField!
//    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var label: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        nextButton.layer.cornerRadius = nextButton.frame.height / 2
//        nextButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tap(recognizer:)))
        label.addGestureRecognizer(tapGesture)
        
    }
    
    func tap(recognizer: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func buttonAction(sender: UIButton!) {
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let tab = storyboard.instantiateViewController(withIdentifier: "TabBarController") as? UITabBarController
//        self.present(tab!, animated: true, completion: {
        
//        })
    }
}

