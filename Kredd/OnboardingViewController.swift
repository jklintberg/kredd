//
//  OnboardingViewController.swift
//  Kredd
//
//  Created by Jonatan Klintberg on 2017-09-14.
//  Copyright © 2017 Jonatan Klintberg. All rights reserved.
//

import UIKit

class OnboardingViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var _scrollView: UIScrollView!
    
    @IBOutlet weak var vc2:PinCodeViewController?
    @IBOutlet weak var vc1:SMSViewController?
    @IBOutlet weak var view1:UIView?
    @IBOutlet weak var view2:UIView?
    @IBOutlet weak var coverImageView:UIImageView?
    @IBOutlet weak var submitButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tap(recognizer:)))
        coverImageView?.addGestureRecognizer(tapGesture)
    
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        vc1 = storyboard.instantiateViewController(withIdentifier: "SMSViewController") as? SMSViewController
        vc2 = (storyboard.instantiateViewController(withIdentifier: "PinCodeViewController") as! PinCodeViewController)
        
        view1 = vc1?.view
        view2 = vc2?.view
        
        submitButton.layer.cornerRadius = submitButton.frame.height / 2
        submitButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        submitButton.layer.shadowColor = UIColor.init(red: 61, green: 59, blue: 130).cgColor
        submitButton.layer.shadowOpacity = 0.3
        submitButton.layer.shadowOffset = CGSize (width: 0, height: 12)
        submitButton.layer.shadowRadius = 15.0

        _scrollView.addSubview(view1!)
        _scrollView.addSubview(view2!)
        _scrollView.contentSize = CGSize (width: view.frame.width * 2, height: view.frame.height)
        
        let scrollTapGesture = UITapGestureRecognizer(target: self, action: #selector(scrollTap(recognizer:)))
        _scrollView?.addGestureRecognizer(scrollTapGesture)
        _scrollView.delegate = self
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            UIView.animate(withDuration: 0.2) {
                self.coverImageView?.alpha = 0
                self.coverImageView?.removeFromSuperview()
            }
        })
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (_scrollView.contentOffset.x < _scrollView.frame.size.width) {
            
            // Page 1
            submitButton.setTitle("Send SMS", for: UIControlState.normal)
            
        } else {
            
            // Page 2
            submitButton.setTitle("Register", for: UIControlState.normal)
            
            
            // Go to tabbar
           
        }
    }
   
    func buttonAction(sender: UIButton!) {
        
        if (_scrollView.contentOffset.x < _scrollView.frame.size.width) {
            // Page 1
             _scrollView.setContentOffset(CGPoint (x: _scrollView.frame.size.width, y: 0), animated: true)
        } else {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tab = storyboard.instantiateViewController(withIdentifier: "TabBarController") as? UITabBarController
            self.present(tab!, animated: true, completion: {
                
            })
        }
        
        
        
       
    }

    func isValidForCheckIn() -> Bool {
        return vc2?._textField.text == "1524"
    }

    override func viewDidLayoutSubviews() {
        
        let f = view.frame
        
        view1!.frame = f
        view2!.frame = CGRect (x: f.width, y: 0, width: f.width, height: f.height)
        
    }
    
    func tap(recognizer: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.2) { 
            self.coverImageView?.alpha = 0
            self.coverImageView?.removeFromSuperview()
        }
    }
    
    func scrollTap(recognizer: UITapGestureRecognizer) {
        print("scroll tapped")
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        coverImageView?.frame = view.frame
    }
}
