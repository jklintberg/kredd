//
//  SuccessViewController.swift
//  Kredd
//
//  Created by Jonatan Klintberg on 2017-09-15.
//  Copyright © 2017 Jonatan Klintberg. All rights reserved.
//

import UIKit
import AVFoundation

class SuccessViewController: UIViewController {
 
    @IBOutlet weak var butt: UIButton!
    @IBOutlet weak var img: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let playerLayer = videoPlayerLayer()
        playerLayer.frame = view.bounds
        view.layer.insertSublayer(playerLayer, at: 0)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.8, execute: {
            UIView.animate(withDuration: 0.3) {
                self.img.alpha = 1
            }
        })
    }
    
    fileprivate var player: AVPlayer? {
        didSet { player?.play() }
    }
    
    fileprivate var playerObserver: Any?
    
    deinit {
        guard let observer = playerObserver else { return }
        NotificationCenter.default.removeObserver(observer)
    }
    
    override func viewDidLayoutSubviews() {
        view.bringSubview(toFront: img)
        view.bringSubview(toFront: butt)
    }
    
    func videoPlayerLayer() -> AVPlayerLayer {
        
        let p = Bundle.main.path(forResource: "iphone", ofType: "mp4")
        let fileURL = URL(fileURLWithPath: p!)
        let player = AVPlayer(url: fileURL)
        let resetPlayer = {
            player.seek(to: kCMTimeZero)
            player.play()
        }
        playerObserver = NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: nil) { notification in
            resetPlayer()
        }
        self.player = player
        return AVPlayerLayer(player: player)
    }
}
